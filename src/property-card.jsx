import React, { useContext } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';

import PropertyContext from './property-context';
import './property-card.css';

const pricePerSqft = (rent, sqft, time) => {
  return `$${((sqft / parseInt(rent.slice(1))) * time).toFixed(2)}`;
};

const PropertyDetails = ({ data }) => (
  <div className="property-card__details">
    <div className="property-card__row-one">
      <div className="property-card__address">
        <span>{data.address1}</span>
        <span>{data.address2}</span>
      </div>
      <span>{data.baseRent}</span>
    </div>
    <div className="property-card__row-two">
      <span>{data.sqft} sqft.</span>
      <span>{pricePerSqft(data.baseRent, data.sqft, 1)} sqft/mo</span>
      <span>{pricePerSqft(data.baseRent, data.sqft, 12)} sqft/year</span>
    </div>
  </div>
);

const PropertyCard = ({ property }) => {
  const { setSelectedProperty } = useContext(PropertyContext)

  return (
    <Card className="property-card" variant="outlined">
      <CardActionArea onClick={() => setSelectedProperty({id: property.id, name: property.name})} >
        <CardContent className="property-card__content">
          <h2>{property.name}</h2>
          <hr />
          <PropertyDetails data={property} />
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default PropertyCard;
