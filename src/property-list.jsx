import React, { useState, useEffect } from 'react';
import './property-list.css';
import PropertyCard from './property-card';

const PropertyList = () => {
  const [properties, setProperties] = useState([]);


  useEffect(() => {
    const url = 'https://talent.ender.com/fe-challenge/properties';
    const body = { token: 'dde70fd6-b600-43cd-b1d9-33250337b31a' };

    const fetchData = async () => {
      try {
        const response = await fetch(url, { method: 'POST', body: JSON.stringify(body) });
        const json = await response.json();

        setProperties(json);
      } catch (error) {
        console.log('error', error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="property-list">
      {properties.map((property) => (
        <PropertyCard key={property.id} property={property} />
      ))}
    </div>
  );
};

export default PropertyList;
