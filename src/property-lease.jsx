import React, { useState, useEffect, useContext } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Popover from '@material-ui/core/Popover';
import PropertyContext from './property-context';

import './property-lease.css';

const ContactDetails = ({contact}) => (
  <div className="property-lease__contact-details">
    <span>Phone: {contact.phone}</span>
    <span>Email: {contact.email}</span>
  </div>
)

const PropertyLease = () => {
  const [leases, setLeases] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [contact, setContact] = React.useState(null);
  const { selectedProperty } = useContext(PropertyContext);

  useEffect(() => {
    const url = `https://talent.ender.com/fe-challenge/properties/${selectedProperty.id}/leases`;
    const body = { token: 'dde70fd6-b600-43cd-b1d9-33250337b31a' };

    const fetchData = async () => {
      try {
        const response = await fetch(url, { method: 'POST', body: JSON.stringify(body) });
        const json = await response.json();
        json.sort((a, b) => a.startDate > b.startDate ? -1 : 1)

        setLeases(json);
      } catch (error) {
        console.log('error', error);
      }
    };

      fetchData();
  }, [selectedProperty]);

  const handleClick = (event, lease) => {
    const primary = getPrimaryContact(lease.contacts);
    const primaryContact = primary ? lease.contacts[primary] : null;

    setContact(primaryContact)

    if(primaryContact) {
      setAnchorEl(event.currentTarget);
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const getPrimaryContact = contacts => Object.keys(contacts).find(contact => contacts[contact].tags.includes("PRIMARY")) || '';

  return (
    <div className="property-lease">
      <h2>{selectedProperty.name}</h2>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: '80%' }} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Tenant</TableCell>
              <TableCell align="right">Start Date</TableCell>
              <TableCell align="right">End Date</TableCell>
              <TableCell align="right">Lease Status</TableCell>
              <TableCell align="right">Primary Contact</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {leases.map((lease) => (
              <TableRow key={lease.id}>
                <TableCell component="th" scope="row">
                  {lease.companyName}
                </TableCell>
                <TableCell align="right">{lease.startDate}</TableCell>
                <TableCell align="right">{lease.inclusiveEndDate}</TableCell>
                <TableCell align="right">{lease.status}</TableCell>
                <TableCell className="property-lease__primary-contact" onClick={(e) => handleClick(e, lease)} align="right">{getPrimaryContact(lease.contacts)}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
      >
        <ContactDetails contact={contact} />
      </Popover>
    </div>
  );
};

export default PropertyLease;
