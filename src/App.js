import React, { useState } from 'react';
import './App.css';
import PropertyContext from './property-context';
import PropertyList from './property-list';
import PropertyLease from './property-lease';

const App = () => {
  const [selectedProperty, setSelectedProperty] = useState();

  return (
    <PropertyContext.Provider value={{ selectedProperty, setSelectedProperty }}>
      <div className="App">
        <PropertyList />
        {selectedProperty && <PropertyLease />}
      </div>
    </PropertyContext.Provider>
  );
}

export default App;
